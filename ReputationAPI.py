import httplib
import os
import hashlib
import ssl

metascan_url = "metascan-online.com"

metascan_type = {
        "hash": "hashlookup.",
        "ip": "ipscan.",
        "url": "ipscan."
        }

metascan_endpoint = {
        "hash": "/v2/hash/",
        "ip": "/v1/scan/",
        "url": "/v1/scan/"
        }

totalhash_query = {
        "hash": "hash:",
        "ip": "ip:",
        "url": "url:"
        }
        
totalhash_url = "api.totalhash.com"

totalhash_endpoint = "/search/"

class MetascanAPI:

    api_key = ""

    def __init__(self,api_key,scan_type):
        self.api_key = api_key
        self.scan_type = scan_type
        self.url = metascan_type[scan_type] + metascan_url
        self.api_endpoint = metascan_endpoint[scan_type]

    def hashLookup(self,hash):
        r = self.makeRequest(hash)
        return r

    def ipLookup(self,ip):
        r = self.makeRequest(ip)
        return r

    def urlLookup(self,url):
        r = self.makeRequest(url)
        return r

    def makeRequest(self,query):
        headers = { "apikey": self.api_key }

        r = httplib.HTTPSConnection(self.url,443)
        r.request("GET",self.api_endpoint + query,headers=headers)
        res = r.getresponse()
        return res.read()

class TotalHashAPI:
    api_key = ""

    def __init__(self,api_key,userid,scan_type,query):
        self.api_key = api_key
        self.userid = userid
        self.query = totalhash_query[scan_type] + query
        self.sign_string = self.query + self.api_key
        self.hash_object = hashlib.sha256(self.sign_string)
        self.hex_dig = self.hash_object.hexdigest()

    def hashLookup(self,hash):
        query = totalhash_query["hash"] + hash
        r = self.makeRequest(query)
        return r

    def urlLookup(self,url):
        pass

    def useragentLookup(self,useragent):
        pass

    def ipLookup(self,ip):
        pass
        
    def mutexLookup(self,mutex):
        pass

    def makeRequest(self,query):
        query_string = query + "&id=" + self.userid + "&sign=" + self.hex_dig
        r = httplib.HTTPSConnection(totalhash_url,context=ssl._create_unverified_context())
        r.request("GET",totalhash_endpoint,query_string)
        res = r.getresponse()
        return res.read()
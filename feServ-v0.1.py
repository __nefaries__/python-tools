##############################################
#
# feServ v0.1
# Description:  Listens for Malware Object events generated
# by FireEye wMPS and MAS appliances and securely
# copies them off to a designated location.
# Author:  Josh Tornetta
##############################################

import cgi
from xml.dom.minidom import parseString
from xml.etree import ElementTree
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from subprocess import Popen, PIPE
import hashlib
import os
import argparse
import sys
import socket

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):

    pass


class ThreadedHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        print("{} connected on port {}".format(self.client_address[0],
               self.client_address[1]))

        # Determine the type of content and how much we need to read in
        ctype = self.headers.getheader("content-type")
        content_len = int(self.headers.getheader("content-length"))
        if ctype == "multipart/form-data":
            self.post_body = cgi.parse_multipart(self.rfile)
        elif ctype == "application/x-www-form-urlencoded":
            self.post_body = self.rfile.read(content_len)
        elif ctype == "text/xml":
            self.post_body = self.rfile.read(content_len)
        else:
            self.post_body = ""

        # Got the data, close the connection by sending a HTTP 200 OK
        self.done(200)
        # Pass the POST content off to be parsed
        xml_handle = XMLHandler()
        xml_handle.handle_xml(self.post_body)

    def done(self, code):

        self.send_response(code)
        self.end_headers()
        print("Connection from {} on port {} closed.".format(self.client_address[0],
               self.client_address[1]))


class BinaryHandler():

    def handle_binary(self, event_id, sensor_id, event_md5sum, binary_name):
        self.user = "admin"
        self.sensor = sensor_id
        self.sensor_path = "/data/malware/done/"
        self.orig_binary_name = binary_name
        self.orig_binary_extension = self.orig_binary_name.split(".")[1]
        # Needs to be updated for storage server path
        self.store_path = " "
        self.binary_name = "EVENT-ID-" + event_id + "-" + event_md5sum + "." + self.orig_binary_extension

        with open(os.devnull, 'w') as devnull:
            p = Popen(["scp", "{user}@{sensor}:{sensor_path}{orig_binary_name}".format(**vars(self)),
                            self.store_path + self.binary_name], stdout=devnull, stderr=PIPE)
            data, error = p.communicate()

        if p.returncode > 0 and error.startswith("ssh"):
            print("ERROR - Unable to resolve hostname {sensor}".format(**vars(self)))
        elif p.returncode > 0:
            print("ERROR - File {sensor_path}{orig_binary_name} not found on sensor {sensor}".format(**vars(self)))
        elif p.returncode < 0:
            print("ERROR - Process {} terminated by signal".format(p.pid))
        else:
            self.hash_cmp(self.store_path, self.binary_name, event_md5sum)

    def hash_cmp(self, store_path, binary_name, md5):
        self.md5_original = md5
        self.binary = store_path + binary_name

        try:
            with open(self.binary) as bin:
                data = bin.read()
                self.md5_ver = hashlib.md5(data).hexdigest()

            if self.md5_original == self.md5_ver:
                pass
            else:
                print("MD5 verification of {binary} failed, removing binary".format(**vars(self)))
                os.remove(self.binary)
        except IOError:
            print("File {binary} not found.".format(**vars(self)))


class XMLHandler():

    def handle_xml(self, post_body):

        # Parse the body of HTTP POST content
        xml_dom = parseString(post_body)
        # Grab the main data objects and ingest as XML
        xml_alert_element= xml_dom.getElementsByTagName('alert')[0].toxml()
        xml_alerts_element = xml_dom.getElementsByTagName('alerts')[0].toxml()
        xml_md5sum_element = xml_dom.getElementsByTagName('md5sum')[0].toxml()
        # Grab the root elements we care about
        sensor = ElementTree.fromstring(xml_alerts_element)
        event = ElementTree.fromstring(xml_alert_element)
        # Grab the attributes from each root element that we care about
        sensor_id = sensor.attrib["appliance"]
        sensor_type = sensor.attrib["product"]
        event_md5sum = ElementTree.fromstring(xml_md5sum_element).text
        event_id = event.attrib["id"]

        # Jump to the appropriate handling based on appliance type
        if sensor_type == "Web MPS":
            self.wmps_handle_xml(sensor_id, event_md5sum, event_id)
        elif sensor_type == "MAS":
            self.mas_handle_xml(sensor_id, event_md5sum, event_id)
        else:
            print("Unknown sensor type {}".format(sensor_type))

    def wmps_handle_xml(self, sensor_id, event_md5sum, event_id):
        binary_name = ".".join([event_md5sum, "zip"])

        binary_handle = BinaryHandler()
        binary_handle.handle_binary(event_id, sensor_id, event_md5sum, binary_name)

    def mas_handle_xml(self, sensor_id, event_md5sum, event_id):
        binary_name = ".".join([event_id, "malware"])

        binary_handle = BinaryHandler()
        binary_handle.handle_binary(event_id, sensor_id, event_md5sum, binary_name)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "feServ - Listens for incoming HTTP POST events from FireEye appliances and securely copies the event binary to a defined location.  Requires Python 2.7+ on Unix")
    parser.add_argument("-p", metavar="PORT", type=int, help="Listening port for incoming events. This is a required option.", required=True)
    parser.add_argument("-l", metavar="LISTEN", help="IP the server listens on for incoming events. This is a required option.", required=True)
    args = parser.parse_args()

    if (sys.version_info >= (2, 7) and sys.version_info < (3, 0)):
        try:
            ThreadedHTTPServer((args.l, args.p), ThreadedHTTPRequestHandler).serve_forever()
        except TypeError:
            print("Error:  Insufficient arguments passed to server.")
            sys.exit(1)
        except socket.error:
            print("Error: Unable to initiate server on requested port or IP.  Check parameters and try again.")
            sys.exit(1)
        except KeyboardInterrupt:
            pass
    else:
        print("ERROR:  feServ requires Python 2.7.X.  This script is not compatible with Python 3.X.")
        sys.exit(1)

from ReputationAPI import MetascanAPI, TotalHashAPI
import sys
import json

metascan_api_key = "72bce8cd8b07b9ef3b388eb6c6f97a89"
totalhash_api_key = "6156d648be0489cbab8350cde53238b3ecba9ca77da2f681a35cfe6b76ccac28"
totalhash_userid = "jtornetta"

def metascan(query):
    metascan = MetascanAPI(metascan_api_key,scan_type)

    if scan_type == "hash":
        rep = metascan.hashLookup(query)
    elif scan_type == "ip":
        rep = metascan.ipLookup(query)
    elif scan_type == "url":
        rep = metascan.urlLookup(query)

    if rep:
        return json.loads(rep)
    else:
        return None

def totalhash(query):
    totalhash = TotalHashAPI(totalhash_api_key,totalhash_userid,scan_type,query)

    if scan_type == "hash":
        rep = totalhash.hashLookup(query)
    elif scan_type == "ip":
        rep = totalhash.ipLookup(query)
    elif scan_type == "url":
        rep = totalhash.urlLookup(query)

    return rep

if __name__ == "__main__":
    reputation_source = sys.argv[1]
    scan_type = sys.argv[2]
    query = sys.argv[3]
    
    if reputation_source == "metascan":
        reputation = metascan(query)

        if scan_type == "hash" and reputation is not None:

            print("Hash uploaded to {} on {}\n\n".format(reputation_source,reputation["file_info"]["upload_timestamp"]))

            for k,v in reputation["scan_results"]["scan_details"].iteritems():
                v = reputation["scan_results"]["scan_details"][k]["threat_found"]
                if v:
                    print("Vendor: {}\nThreat: {}\nDat File Date: {}\n".format(k,v,reputation["scan_results"]["scan_details"][k]["def_time"]))
        elif scan_type == "ip" and reputation is not None:
            print(json.dumps(reputation,indent=4,separators=(",",":"),sort_keys=False))
        elif scan_type == "url" and reputation is not None:
            print(json.dumps(reputation,indent=4,separators=(",",":"),sort_keys=False))
        else:
            print("No Result")
    elif reputation_source == "totalhash":
        reputation = totalhash(query)
        print reputation
    else:
        pass